﻿using System;

namespace Gevlee.RestBets.DataAccess.Entities
{
    public class EntityBase
    {
        public int Id { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}