﻿namespace Gevlee.RestBets.DataAccess.Entities
{
    public class BetEntry : EntityBase
    {
        public Bet Bet { get; set; }

        public Team Team { get; set; }

        public int Score { get; set; }
    }
}