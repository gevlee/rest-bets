﻿using System.Collections.Generic;

namespace Gevlee.RestBets.DataAccess.Entities
{
    public class Bet : EntityBase
    {
        public Match Match { get; set; }

        public string Bettor { get; set; }

        public ICollection<BetEntry> Entries { get; set; }
    }
}