﻿namespace Gevlee.RestBets.DataAccess.Entities
{
    public class Team : EntityBase
    {
        public string Name { get; set; }
    }
}