﻿using System;
using System.Collections.Generic;

namespace Gevlee.RestBets.DataAccess.Entities
{
    public class Match : EntityBase
    {
        public ICollection<Team> Teams { get; set; }

        public ICollection<TeamScore> Scores { get; set; }

        public DateTime PlayDate { get; set; }

    }
}