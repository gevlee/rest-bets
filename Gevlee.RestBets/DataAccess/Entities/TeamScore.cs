﻿namespace Gevlee.RestBets.DataAccess.Entities
{
    public class TeamScore : EntityBase
    {
        public Team Team { get; set; }

        public Match Match { get; set; }

        public int Value { get; set; }
    }
}