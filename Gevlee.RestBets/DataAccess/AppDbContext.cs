﻿using Microsoft.EntityFrameworkCore;

namespace Gevlee.RestBets.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }
    }
}